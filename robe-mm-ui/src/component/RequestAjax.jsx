import React from "react";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import AjaxRequest from "robe-react-commons/lib/connections/AjaxRequest";
export default  class RequestAjax extends ShallowComponent{

    static propTypes :Map ={
        data:React.PropTypes.object.isRequired
    };
    constructor(props){
        super(props);
        let data = this.props.data;
        this.state = {
            urlAjax:data.urlAjax
        }
        }
        render(){
        return({

        });
}
componentDidMount(){
    let url = this.state.urlAjax;
    let me = this;

    url.request.forEach(function (v,i) {
        if(v.type =="GET"){
            let req = new AjaxRequest({
                url:v.url,
                type:v.type
            });
            req.call(undefined, undefined, (response) => {

                let state = {};
                let field = v.fieldName;
                state.items = response;
                state.propsOfFields = me.state.propsOfFields;
                for (let i = 0; i < response.length; i++) {
                    let res = response[i];
                    state.propsOfFields[field].items.push({
                        value: res.oid,
                        text: res[v.showName]
                    });
                }
                me.setState(state);
                me.forceUpdate();
            });
        }
    });
}

}