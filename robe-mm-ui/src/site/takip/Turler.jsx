import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button} from "react-bootstrap";
import Model from "./TurlerModel.json";
import TextInput from "robe-react-ui/lib/inputs/TextInput";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";

export default class Turler extends ShallowComponent {
    static idField = "";

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-harcamaturleris",
                read: {
                    url: "dao-harcamaturleris"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            turAdi: "",
            version: "",
            fields: Model.fields,
            store: store,
            propsOfFields: {}
        };
    }

    render() {
        return (
            <Card header="HARCAMA VERİ TÜRLERİ YÖNETİMİ EKRANI">
                <div>
                    <TextInput
                        label="TÜR ADI"
                        name="turAdi"
                        value={this.state.turAdi}
                        onChange={this.__handleChangeInputText}
                    />
                    <Button onClick={this.__onSaveUpdate}>Kaydet</Button>
                    <Button onClick={this.__new}>İptal</Button>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        cellRenderer={this.__cellRenderer}
                        searchable={false}
                    />
                </div>
            </Card>
        );
    }

    __controller() {
        if (this.state.turAdi == "") {
            Toast.error("Lütfen tür adı giriniz!");
            return false;
        } else {
            return true;
        }
    }

    //noinspection JSAnnotator
    __onSaveUpdate(e: Object) {
        if (this.__controller() == false) {
            return;
        }
        if (this.state.oid == "") {
            let jsonSave = {
                turAdi: this.state.turAdi,
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                turAdi: this.state.turAdi,
                version: this.state.version,
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.__new();
    }

    //noinspection JSAnnotator
    __handleChangeInputText(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    __new() {
        this.setState({oid: ""});
        this.setState({turAdi: ""});
        this.setState({version: ""});
        this.setState({
            SingleSelect: {
                value: "",
                text: ""
            }
        });
    }

    __edit() {
        let selectedRows = this.refs.table.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            oid: selectedRows[0].oid,
            turAdi: selectedRows[0].turAdi,
            version: selectedRows[0].version
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    __remove() {
        let selectedRows = this.refs.table.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    //noinspection JSAnnotator
    __cellRenderer(idx: number, fields: Array, row: Object) {
        if (fields[idx].name == 'turAdi') {
            return <td key={fields[idx].name}>{row.turAdi}</td>;
        }
    }

}