import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button} from "react-bootstrap";
import Model from "./MarketModel.json";
import TextInput from "robe-react-ui/lib/inputs/TextInput";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import SelectInput from "robe-react-ui/lib/inputs/SelectInput";
import AjaxRequest from "robe-react-commons/lib/connections/AjaxRequest";
import NumericInput from "robe-react-ui/lib/inputs/NumericInput";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";


export default class Marketler extends ShallowComponent {
    static idField = "";
    ilRequest = new AjaxRequest({
        url: "dao-ils",
        type: "GET"
    });

    sirketRequest = new AjaxRequest({
        url: "dao-sirketlers",
        type: "GET"
    });

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-marketlers",
                read: {
                    url: "dao-marketlers"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            marketAdi: "",
            m2: "",
            version: "",
            fields: Model.fields,
            store: store,
            IlSelect: {},
            IlceSelect: {},
            SirketSelect: {},
            propsOfFields: {
                ilOid: {
                    items: []
                },
                ilceOid: {
                    items: []
                },
                sirketOid: {
                    items: []
                }
            },
            itemIl: [],
            itemIce: [],
            itemSirket: []
        };
    }

    render() {
        return (
            <Card header="MARKET YÖNETİMİ EKRANI">
                <div>
                    <SelectInput
                        label="İL"
                        name="IlSelect"
                        items={this.state.itemIl}
                        textField="ilAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.IlSelect.value}
                        onChange={this.__handleChangeDropdownIl}
                    />
                    <SelectInput
                        label="İLÇE"
                        name="IlceSelect"
                        items={this.state.itemIlce}
                        textField="ilceAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.IlceSelect.value}
                        onChange={this.__handleChangeDropdownIlce}
                    />
                    <SelectInput
                        label="ŞİRKET"
                        name="SirketSelect"
                        items={this.state.itemSirket}
                        textField="sirketAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.SirketSelect.value}
                        onChange={this.__handleChangeDropdownSirket}
                    />
                    <TextInput
                        label="MARKET ADI"
                        name="marketAdi"
                        value={this.state.marketAdi}
                        onChange={this.__handleChangeInputText}
                    />
                    <NumericInput
                        label="MARKET m2"
                        name="m2"
                        value={this.state.m2}
                        onChange={this.__handleChangeInputText}
                        placeholder="Sadece sayı giriniz."
                    />
                    <Button onClick={this.__onSaveUpdate}>Kaydet</Button>
                    <Button onClick={this.__new}>İptal</Button>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        cellRenderer={this.__cellRenderer}
                        searchable={false}
                    />

                </div>
            </Card>
        );
    }

    __new() {
        this.setState({oid: ""});
        this.setState({il: ""});
        this.setState({ilce: ""});
        this.setState({sirket: ""});
        this.setState({marketAdi: ""});
        this.setState({m2: ""});
        this.setState({version: ""});
        this.setState({
            IlSelect: {
                value: "",
                text: ""
            }
        });
        this.setState({
            IlceSelect: {
                value: "",
                text: ""
            }
        });
        this.setState({
            SirketSelect: {
                value: "",
                text: ""
            }
        });
    }

    __edit() {
        let selectedRows = this.refs.table.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            oid: selectedRows[0].oid,
            marketAdi: selectedRows[0].marketAdi,
            m2: selectedRows[0].m2,
            il: selectedRows[0].ilce.il,
            ilce: selectedRows[0].ilce,
            sirket: selectedRows[0].sirket,
            version: selectedRows[0].version,
            IlSelect: {
                value: selectedRows[0].ilce.il.oid,
                text: selectedRows[0].ilce.il.ilAdi
            },
            IlceSelect: {
                value: selectedRows[0].ilce.oid,
                text: selectedRows[0].ilce.ilceAdi
            },
            SirketSelect: {
                value: selectedRows[0].sirket.oid,
                text: selectedRows[0].sirket.sirketAdi
            }
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    //noinspection JSAnnotator
    __handleChangeInputText(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    //noinspection JSAnnotator
    __handleChangeDropdownIl(e: Object) {

        let state = {};
        //let value = e.target.value;
        //state[e.target.name] = value;

        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        this.setState({
            [e.target.name]: {
                value: value
            }
        });

        let ilceRequest = new AjaxRequest({
            url: "dao-ilces/" + value,
            type: "GET"
        });

        ilceRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemIlce = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.ilceOid.items.push({
                    value: res.oid,
                    text: res.ilceAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

        this.setState(state);
    }

    //noinspection JSAnnotator
    __handleChangeDropdownIlce(e: Object) {

        let state = {};
        //let value = e.target.value;
        //state[e.target.name] = value;

        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        this.setState({
            [e.target.name]: {
                value: value
            }
        });


        this.setState(state);
    }

    //noinspection JSAnnotator
    __handleChangeDropdownSirket(e: Object) {
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        this.setState({
            SirketSelect: {
                value: value
            }
        });
    }

    __controller() {
        if (this.state.marketAdi == "") {
            Toast.error("Lütfen market adı giriniz!");
            return false;
        } else if (this.state.m2 == "") {
            Toast.error("Lütfen metre kare giriniz!");
            return false;
        } else {
            return true;
        }
    }

    //noinspection JSAnnotator
    __onSaveUpdate(e: Object) {
        if (this.__controller() == false) {
            return;
        }
        let selectedIlceOid = this.state.IlceSelect.value;
        let selectedSirketOid = this.state.SirketSelect.value;
        if (this.state.oid == "") {
            let jsonSave = {
                marketAdi: this.state.marketAdi,
                m2: this.state.m2,
                ilce: this.__findIlceObject(selectedIlceOid),
                sirket: this.__findSirketObject(selectedSirketOid)
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                marketAdi: this.state.marketAdi,
                m2: this.state.m2,
                version: this.state.version,
                ilce: this.__findIlceObject(selectedIlceOid),
                sirket: this.__findSirketObject(selectedSirketOid)
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.__new();
    }

    __remove() {
        let selectedRows = this.refs.table.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    componentDidMount() {

        this.ilRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemIl = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.ilOid.items.push({
                    value: res.oid,
                    text: res.ilAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

        this.sirketRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemSirket = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.sirketOid.items.push({
                    value: res.oid,
                    text: res.sirketAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));


    };

    //noinspection JSAnnotator
    __cellRenderer(idx: number, fields: Array, row: Object) {
        if (fields[idx].name == 'ilOid') {
            if (row.ilce.il == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.ilce.il.ilKodu} - {row.ilce.il.ilAdi}</td>;
        }
        if (fields[idx].name == 'ilceOid') {
            if (row.ilce == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.ilce.ilceKodu} - {row.ilce.ilceAdi}</td>;
        }
        if (fields[idx].name == 'sirketOid') {
            if (row.sirket == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.sirket.sirketAdi}</td>;
        }
        if (fields[idx].name == 'marketAdi') {
            return <td key={fields[idx].name}>{row.marketAdi}</td>;
        }
        if (fields[idx].name == 'm2') {
            return <td key={fields[idx].name}>{row.m2}</td>;
        }
    }

    //noinspection JSAnnotator
    __findIlObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemIl.length; i++) {
            let object = this.state.itemIl[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }

    //noinspection JSAnnotator
    __findIlceObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemIlce.length; i++) {
            let object = this.state.itemIlce[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }

    //noinspection JSAnnotator
    __findSirketObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemSirket.length; i++) {
            let object = this.state.itemSirket[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }
}