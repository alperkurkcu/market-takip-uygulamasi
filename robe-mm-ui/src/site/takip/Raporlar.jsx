import React from "react";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Card from "../../card/Card";
import {CartesianGrid, Legend, Line, LineChart, ReferenceLine, Tooltip, XAxis, YAxis} from "recharts";
import SelectInput from "robe-react-ui/lib/inputs/SelectInput";
import Model from "./BilgilendirmeModel.json";
import AjaxRequest from "robe-react-commons/lib/connections/AjaxRequest";
import {Col} from "react-bootstrap";

export default class Raporlar extends ShallowComponent {

    marketRequest = new AjaxRequest({
        url: "dao-marketlers",
        type: "GET"
    });

    deger2016 = 0;
    deger2017 = 0;
    deger2018 = 0;
    deger2019 = 0;
    deger2020 = 0;

    constructor(props) {
        super(props);

        const dataYear = [
            {
                id: 2016,
                name: "2016",
            },
            {
                id: 2017,
                name: "2017",
            },
            {
                id: 2018,
                name: "2018",
            },
            {
                id: 2019,
                name: "2019",
            },
            {
                id: 2020,
                name: "2020",
            }
        ];

        this.state = {
            item: {},
            dataSicaklik: [],
            dataKw: [],
            dataYear: dataYear,
            MarketSelect: {},
            YilSelect: {},
            fields: Model.fields,
            propsOfFields: {
                marketOid: {
                    items: []
                },
                turOid: {
                    items: []
                }
            },
            itemMarket: [],
            itemInfo: [],
            infoDescription: null
        };
    }

    render() {
        return (
            <Card header="RAPORLAMALAR">
                <Col md={12}>
                    <Col md={6}>
                        <SelectInput
                            label="YILLAR"
                            name="YilSelect"
                            items={this.state.dataYear}
                            textField="name"
                            valueField="id"
                            readOnly={true}
                            value={this.state.YilSelect.value}
                            onChange={this.__handleChangeDropdown}
                        />
                    </Col>
                    <Col md={6}>
                        <SelectInput
                            label="MARKET"
                            name="MarketSelect"
                            items={this.state.itemMarket}
                            textField="marketAdi"
                            valueField="oid"
                            readOnly={true}
                            value={this.state.MarketSelect.value}
                            onChange={this.__handleChangeDropdownMarket}
                        />
                    </Col>
                </Col>
                <tr>
                    <th>BİLGİLENDİRME</th>
                </tr>
                <tr>
                    <td>{this.state.infoDescription}</td>
                </tr>
                {/*<Card header="BİLGİLENDİRME">
                 <Col md={12} xs={6}>
                 <p>{this.state.infoDescription}</p>
                 </Col>
                 </Card>*/}
                <Card header="kW DURUMU">
                    <LineChart width={1000} height={300} data={this.state.dataKw}>
                        <XAxis dataKey="name" padding={{left: 30, right: 30}}/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        <Line type="monotone" dataKey="2016" stroke="blue"/>
                        <Line type="monotone" dataKey="2017" stroke="red" activeDot={{r: 8}}/>
                        <Line type="monotone" dataKey="2018" stroke="green"/>
                        <Line type="monotone" dataKey="2019" stroke="brown"/>
                        <Line type="monotone" dataKey="2020" stroke="orange"/>
                    </LineChart>
                </Card>
                <Card header="ORTAM SICAKLIK DURUMU - C°">
                    <LineChart width={1000} height={300} data={this.state.dataSicaklik}>
                        <XAxis dataKey="name" padding={{left: 30, right: 30}}/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        <Line type="monotone" dataKey="2016" stroke="#82ca9d"/>
                        <Line type="monotone" dataKey="2017" stroke="#8884d8" activeDot={{r: 8}}/>
                        <Line type="monotone" dataKey="2018" stroke="#82ca9d"/>
                        <Line type="monotone" dataKey="2019" stroke="#82ca9d"/>
                        <Line type="monotone" dataKey="2020" stroke="#82ca9d"/>
                        <ReferenceLine y="24" label="Normal Oda Sıcaklığı - 24C°" stroke="red"/>
                        <Tooltip content={this.renderTooltip}/>
                    </LineChart>
                </Card>
            </Card>
        );
    }

    //noinspection JSAnnotator
    __cellRenderer(idx: number, fields: Array, row: Object) {
        if (fields[idx].name == 'marketOid') {
            if (row.marketler == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.marketler.marketAdi}</td>;
        }
        if (fields[idx].name == 'turOid') {
            if (row.turler == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.turler.turAdi}</td>;
        }
        if (fields[idx].name == 'saatAraligi') {
            return <td key={fields[idx].name}>{row.saatAraligi}</td>;
        }
    }

    componentDidMount() {
        this.marketRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemMarket = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.marketOid.items.push({
                    value: res.oid,
                    text: res.marketAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

    };

    //noinspection JSAnnotator
    __handleChangeDropdown(e: Object) {
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        //state[e.target.name] = value;
        //this.setState(state);
        this.setState({
            [e.target.name]: {
                value: value
            }
        });
        this.setState({
            MarketSelect: {
                value: "",
                text: ""
            }
        });
    }

    //noinspection JSAnnotator
    __handleChangeDropdownMarket(e: Object) {

        let state = {};

        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        this.setState({
            [e.target.name]: {
                value: value
            }
        });

        let infoRequest = new AjaxRequest({
            url: "dao-bilgilendirmes/" + value,
            type: "GET"
        });

        infoRequest.call(undefined, undefined, function (response) {
            state.itemInfo = response;
            state.infoDescription = "";
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.infoDescription = state.infoDescription + " " +
                    res.marketler.marketAdi + " şubesi, " + res.turler.turAdi + " " + res.saatAraligi + " arasında çalışmaktadır.\n\n\n\n";
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

        let kwRequest = new AjaxRequest({
            url: "dao-verilers/oid?prmMarketId=" + value + "&prmTurId=3dcc7475ee934bf8b5c74c52369d07d1&prmYilId=" + this.state.YilSelect.value,
            type: "GET"
        });

        let dataKw = [];
        kwRequest.call(undefined, undefined, (response) => {
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Ocak") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Ocak",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Şubat") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Şubat",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Mart") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Mart",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Nisan") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Nisan",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Mayıs") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Mayıs",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Haziran") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Haziran",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Temmuz") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Temmuz",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Ağustos") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Ağustos",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Eylül") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Eylül",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Ekim") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Ekim",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Kasım") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Kasım",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiMonthString === "Aralık") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataKw.push({
                name: "Aralık",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020,
                amt: 30
            });
            this.__Reset();

            this.setState({dataKw: dataKw});
            this.forceUpdate();
        })

        let sicaklikRequest = new AjaxRequest({
            url: "dao-verilers/oid?prmMarketId=" + value + "&prmTurId=e8b18c0c84bb4f09975d28b5a4b626fe&prmYilId=" + this.state.YilSelect.value,
            type: "GET"
        });

        let dataSicaklik = [];
        sicaklikRequest.call(undefined, undefined, (response) => {

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "01") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "01",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "02") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "02",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "03") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "03",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "04") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "04",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "05") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "05",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "06") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "06",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "07") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "07",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "08") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "08",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "09") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "09",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "10") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "10",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "11") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "11",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "12") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "12",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "13") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "13",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "14") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "14",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "15") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "15",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "16") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "16",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "17") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "17",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "18") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "18",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "19") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "19",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "20") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "20",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "21") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "21",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "22") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "22",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "23") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "23",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "24") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "24",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "25") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "25",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "26") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "26",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "27") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "27",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "28") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "28",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "29") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "29",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "30") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "30",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                if (res.kayitTarihiDayString === "31") {
                    if (res.kayitTarihiYearString === "2016") {
                        this.deger2016 = res.deger;
                    } else if (res.kayitTarihiYearString === "2017") {
                        this.deger2017 = res.deger;
                    } else if (res.kayitTarihiYearString === "2018") {
                        this.deger2018 = res.deger;
                    } else if (res.kayitTarihiYearString === "2019") {
                        this.deger2019 = res.deger;
                    } else if (res.kayitTarihiYearString === "2020") {
                        this.deger2020 = res.deger;
                    }
                }
            }
            dataSicaklik.push({
                name: "31",
                2016: this.deger2016,
                2017: this.deger2017,
                2018: this.deger2018,
                2019: this.deger2019,
                2020: this.deger2020
            });
            this.__Reset();

            this.setState({dataSicaklik: dataSicaklik});
            this.forceUpdate();
        })

        this.setState({
            MarketSelect: {
                value: "",
                text: ""
            }
        });
        this.setState({
            YilSelect: {
                value: "",
                text: ""
            }
        });

    }

    __Reset() {
        this.deger2016 = 0;
        this.deger2017 = 0;
        this.deger2018 = 0;
        this.deger2019 = 0;
        this.deger2020 = 0;
    }

    renderTooltip() {
        return (
            <div>Custom content</div>
        )
    }
}