import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button} from "react-bootstrap";
import Model from "./SirketModel.json";
import TextInput from "robe-react-ui/lib/inputs/TextInput";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";

export default class Sirketler extends ShallowComponent {
    static idField = "";

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-sirketlers",
                read: {
                    url: "dao-sirketlers"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            sirketAdi: "",
            version: "",
            fields: Model.fields,
            store: store,
            propsOfFields: {},
            dataKayit: []
        };
    }

    render() {
        return (
            <Card header="ŞİRKET YÖNETİM EKRANI">
                <div>
                    <TextInput
                        label="Şirket Adı"
                        name="sirketAdi"
                        value={this.state.sirketAdi}
                        onChange={this.__handleChange}
                        onKeyPress={this.__onKeyPress}
                        placeholder="Şirket adı giriniz."
                    />
                    <Button onClick={this.__onClick}>Kaydet</Button>
                    <Button onClick={this.__onCancel}>İptal</Button>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table1"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        searchable={false}
                    />
                </div>
            </Card>
        );
    }

    //noinspection JSAnnotator
    __onClick(e: Object) {
        if (this.state.sirketAdi == "") {
            Toast.error("Lütfen şirket adı giriniz!");
            return;
        }
        if (this.state.oid == "") {
            let jsonSave = {
                sirketAdi: this.state.sirketAdi,
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                sirketAdi: this.state.sirketAdi,
                version: this.state.version
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.setState({oid: ""});
        this.setState({sirketAdi: ""});
        this.setState({version: ""});
    }

    //noinspection JSAnnotator
    __handleChange(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    __new() {
        this.setState({oid: ""});
        this.setState({sirketAdi: ""});
        this.setState({version: ""});
    }

    __edit() {
        let selectedRows = this.refs.table1.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            oid: selectedRows[0].oid,
            sirketAdi: selectedRows[0].sirketAdi,
            version: selectedRows[0].version
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    __onCancel() {
        this.setState({oid: ""});
        this.setState({sirketAdi: ""});
        this.setState({version: ""});
    }

    __remove() {
        let selectedRows = this.refs.table1.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    __onKeyPress(e) {
        if (e.key == "Enter")
            this.__onClick();
    }
}