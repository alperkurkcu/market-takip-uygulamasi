import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button} from "react-bootstrap";
import Model from "./IlcelerModel.json";
import TextInput from "robe-react-ui/lib/inputs/TextInput";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import SelectInput from "robe-react-ui/lib/inputs/SelectInput";
import AjaxRequest from "robe-react-commons/lib/connections/AjaxRequest";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";

export default class Ilceler extends ShallowComponent {
    static idField = "";
    ilRequest = new AjaxRequest({
        url: "dao-ils",
        type: "GET"
    });

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-ilces",
                read: {
                    url: "dao-ilces"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            ilceKodu: "",
            ilceAdi: "",
            version: "",
            fields: Model.fields,
            store: store,
            SingleSelect: {},
            propsOfFields: {
                ilOid: {
                    items: []
                }
            },
            itemIl: []
        };
    }

    render() {
        return (
            <Card header="İLÇE YÖNETİMİ EKRANI">
                <div>
                    <SelectInput
                        label="İL"
                        name="SingleSelect"
                        items={this.state.itemIl}
                        textField="ilAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.SingleSelect.value}
                        onChange={this.__handleChangeDropdown}
                    />
                    <TextInput
                        label="İLÇE KODU"
                        name="ilceKodu"
                        value={this.state.ilceKodu}
                        onChange={this.__handleChangeInputText}
                    />
                    <TextInput
                        label="İLÇE ADI"
                        name="ilceAdi"
                        value={this.state.ilceAdi}
                        onChange={this.__handleChangeInputText}
                    />
                    <Button onClick={this.__onSaveUpdate}>Kaydet</Button>
                    <Button onClick={this.__new}>İptal</Button>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        cellRenderer={this.__cellRenderer}
                        searchable={false}
                    />
                </div>
            </Card>
        );
    }

    __controller() {
        if (this.state.ilceKodu == "") {
            Toast.error("Lütfen ilçe kodu giriniz!");
            return false;
        } else if (this.state.ilceAdi == "") {
            Toast.error("Lütfen ilçe adı giriniz!");
            return false;
        } else {
            return true;
        }
    }

    //noinspection JSAnnotator
    __onSaveUpdate(e: Object) {
        if (this.__controller() == false) {
            return;
        }
        let selectOid = this.state.SingleSelect.value;
        if (this.state.oid == "") {
            let jsonSave = {
                ilceKodu: this.state.ilceKodu,
                ilceAdi: this.state.ilceAdi,
                il: this.__findIlObject(selectOid)
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                ilceKodu: this.state.ilceKodu,
                ilceAdi: this.state.ilceAdi,
                version: this.state.version,
                il: this.__findIlObject(selectOid)
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.__new();
    }

    //noinspection JSAnnotator
    __handleChangeInputText(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    //noinspection JSAnnotator
    __handleChangeDropdown(e: Object) {
        //let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        //state[e.target.name] = value;
        //this.setState(state);
        this.setState({
            SingleSelect: {
                value: value
            }
        });
    }

    __new() {
        this.setState({oid: ""});
        this.setState({il: ""});
        this.setState({ilceKodu: ""});
        this.setState({ilceAdi: ""});
        this.setState({version: ""});
        this.setState({
            SingleSelect: {
                value: "",
                text: ""
            }
        });
    }

    __edit() {
        let selectedRows = this.refs.table.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            oid: selectedRows[0].oid,
            ilceKodu: selectedRows[0].ilceKodu,
            ilceAdi: selectedRows[0].ilceAdi,
            il: selectedRows[0].il,
            version: selectedRows[0].version,
            SingleSelect: {
                value: selectedRows[0].il.oid,
                text: selectedRows[0].il.ilAdi
            }
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    __remove() {
        let selectedRows = this.refs.table.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    componentDidMount() {

        this.ilRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemIl = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.ilOid.items.push({
                    value: res.oid,
                    text: res.ilAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

    };

    //noinspection JSAnnotator
    __cellRenderer(idx: number, fields: Array, row: Object) {
        if (fields[idx].name == 'ilceAdi') {
            return <td key={fields[idx].name}>{row.ilceAdi}</td>;
        }
        if (fields[idx].name == 'ilceKodu') {
            return <td key={fields[idx].name}>{row.ilceKodu}</td>;
        }
        if (fields[idx].name == 'ilOid') {
            if (row.il == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.il.ilKodu} - {row.il.ilAdi}</td>;
        }

    }

    //noinspection JSAnnotator
    __findIlObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemIl.length; i++) {
            let object = this.state.itemIl[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }
}