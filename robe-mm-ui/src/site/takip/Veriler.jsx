import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button} from "react-bootstrap";
import Model from "./VerilerModel.json";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import SelectInput from "robe-react-ui/lib/inputs/SelectInput";
import AjaxRequest from "robe-react-commons/lib/connections/AjaxRequest";
import DateInput from "robe-react-ui/lib/inputs/DateInput";
import NumericInput from "robe-react-ui/lib/inputs/NumericInput";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";


export default class Marketler extends ShallowComponent {
    static idField = "";
    marketRequest = new AjaxRequest({
        url: "dao-marketlers",
        type: "GET"
    });
    turRequest = new AjaxRequest({
        url: "dao-veriturleris",
        type: "GET"
    });

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-verilers",
                read: {
                    url: "dao-verilers"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            deger: "",
            kayitTarihi: "",
            version: "",
            fields: Model.fields,
            store: store,
            MarketSelect: {},
            TurSelect: {},
            propsOfFields: {
                marketOid: {
                    items: []
                },
                turOid: {
                    items: []
                }
            },
            itemMarket: [],
            itemTur: []
        };
    }

    render() {
        return (
            <Card header="VERİ EKLEME EKRANI">
                <div>
                    <SelectInput
                        label="MARKET"
                        name="MarketSelect"
                        items={this.state.itemMarket}
                        textField="marketAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.MarketSelect.value}
                        onChange={this.__handleChangeDropdown}
                    />
                    <SelectInput
                        label="VERİ TÜRÜ"
                        name="TurSelect"
                        items={this.state.itemTur}
                        textField="turAdi"
                        valueField="oid"
                        readOnly={true}
                        value={this.state.itemTur.value}
                        onChange={this.__handleChangeDropdown}
                    />
                    <NumericInput
                        label="DEĞER"
                        name="deger"
                        value={this.state.deger}
                        onChange={this.__handleChangeInputText}
                    />
                    <DateInput
                        label="VERİ TARİHİ"
                        name="kayitTarihi"
                        value={this.state.kayitTarihi}
                        onChange={this.__handleChangeInputText}
                        validations={{
                            required: true
                        }}
                        locale="tr"
                        format="DD MM YYYY"
                    />
                    <Button onClick={this.__onSaveUpdate}>Kaydet</Button>
                    <Button onClick={this.__new}>İptal</Button>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        cellRenderer={this.__cellRenderer}
                        searchable={false}
                    />
                </div>
            </Card>
        );
    }

    __new() {
        this.setState({oid: ""});
        this.setState({deger: ""});
        this.setState({kayitTarihi: ""});
        this.setState({version: ""});
        this.setState({
            MarketSelect: {
                value: "",
                text: ""
            }
        });
        this.setState({
            TurSelect: {
                value: "",
                text: ""
            }
        });
    }

    __edit() {
        let selectedRows = this.refs.table.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            oid: selectedRows[0].oid,
            deger: selectedRows[0].deger,
            kayitTarihi: selectedRows[0].kayitTarihi,
            market: selectedRows[0].market,
            veriTuru: selectedRows[0].veriTuru,
            version: selectedRows[0].version,
            MarketSelect: {
                value: selectedRows[0].market.oid,
                text: selectedRows[0].market.marketAdi
            },
            TurSelect: {
                value: selectedRows[0].veriTuru.oid,
                text: selectedRows[0].veriTuru.turAdi
            }
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    //noinspection JSAnnotator
    __handleChangeInputText(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    //noinspection JSAnnotator
    __handleChangeDropdown(e: Object) {
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.key;
        //state[e.target.name] = value;
        //this.setState(state);
        this.setState({
            [e.target.name]: {
                value: value
            }
        });
    }

    __controller() {
        if (this.state.deger == "") {
            Toast.error("Lütfen değer giriniz!");
            return false;
        } else if (this.state.kayitTarihi == "") {
            Toast.error("Lütfen tarih giriniz!");
            return false;
        } else {
            return true;
        }
    }

    //noinspection JSAnnotator
    __onSaveUpdate(e: Object) {
        if (this.__controller() == false) {
            return;
        }
        let selectedMarketOid = this.state.MarketSelect.value;
        let selectedTurOid = this.state.TurSelect.value;
        if (this.state.oid == "") {
            let jsonSave = {
                deger: this.state.deger,
                kayitTarihi: this.state.kayitTarihi,
                market: this.__findMarketObject(selectedMarketOid),
                veriTuru: this.__findTurObject(selectedTurOid)
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                deger: this.state.deger,
                kayitTarihi: this.state.kayitTarihi,
                version: this.state.version,
                market: this.__findMarketObject(selectedMarketOid),
                veriTuru: this.__findTurObject(selectedTurOid)
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.__new();
    }

    __remove() {
        let selectedRows = this.refs.table.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    componentDidMount() {

        this.marketRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemMarket = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.marketOid.items.push({
                    value: res.oid,
                    text: res.marketAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));

        this.turRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.itemTur = response;
            state.propsOfFields = this.state.propsOfFields;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.propsOfFields.turOid.items.push({
                    value: res.oid,
                    text: res.turAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));
    };

    //noinspection JSAnnotator
    __cellRenderer(idx: number, fields: Array, row: Object) {
        if (fields[idx].name == 'marketOid') {
            if (row.market == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.market.marketAdi}</td>;
        }
        if (fields[idx].name == 'turOid') {
            if (row.veriTuru == null)
                return <td key={fields[idx].name}>{""}</td>;
            else
                return <td key={fields[idx].name}>{row.veriTuru.turAdi}</td>;
        }
        if (fields[idx].name == 'deger') {
            return <td key={fields[idx].name}>{row.deger}</td>;
        }
        if (fields[idx].name == 'kayitTarihi') {
            return <td key={fields[idx].name}>{row.kayitTarihiString}</td>;
        }
    }

    //noinspection JSAnnotator
    __findMarketObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemMarket.length; i++) {
            let object = this.state.itemMarket[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }

    //noinspection JSAnnotator
    __findTurObject(selectedOid: String) {
        for (let i = 0; i < this.state.itemTur.length; i++) {
            let object = this.state.itemTur[i];
            if (object && object.oid === selectedOid)
                return object;
        }
        return undefined;
    }
}