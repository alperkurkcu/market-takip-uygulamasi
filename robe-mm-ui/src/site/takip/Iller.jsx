import React from "react";
import DataGrid from "robe-react-ui/lib/datagrid/DataGrid";
import {Button, Panel} from "react-bootstrap";
import Model from "./IllerModel.json";
import TextInput from "robe-react-ui/lib/inputs/TextInput";
import RemoteEndPoint from "robe-react-commons/lib/endpoint/RemoteEndPoint";
import ShallowComponent from "robe-react-commons/lib/components/ShallowComponent";
import Store from "robe-react-commons/lib/stores/Store";
import Toast from "robe-react-ui/lib/toast/Toast";
import Card from "../../card/Card";

export default class Iller extends ShallowComponent {
    static idField = "";

    constructor(props) {
        super(props);

        let store = new Store({
            endPoint: new RemoteEndPoint({
                url: "dao-ils",
                read: {
                    url: "dao-ils"
                }
            }),
            idField: "oid",
            autoLoad: true
        });

        this.state = {
            oid: "",
            ilKodu: "",
            ilAdi: "",
            version: "",
            open: false,
            fields: Model.fields,
            store: store,
            propsOfFields: {},
            dataKayit: []
        };
    }

    render() {
        return (
            <Card header="İL YÖNETİM EKRANI">
                <div>
                    <Panel collapsible expanded={this.state.open}>
                        <TextInput
                            label="İl Kodu"
                            name="ilKodu"
                            value={this.state.ilKodu}
                            onChange={this.__handleChange}
                            onKeyPress={this.__onKeyPress}
                            placeholder="İl Kodu"
                        />
                        <TextInput
                            label="İl Adı"
                            name="ilAdi"
                            value={this.state.ilAdi}
                            onChange={this.__handleChange}
                            onKeyPress={this.__onKeyPress}
                            placeholder="İl Adı"
                        />
                        <Button onClick={this.__onClick}>Kaydet</Button>
                        <Button onClick={this.__onCancel}>İptal</Button>
                    </Panel>
                    <DataGrid
                        fields={this.state.fields}
                        store={this.state.store}
                        propsOfFields={this.state.propsOfFields}
                        ref="table1"
                        toolbar={[{name: "create"}, {name: "edit"}, {name: "delete"}]}
                        onNewClick={this.__new}
                        onEditClick={this.__edit}
                        onDeleteClick={this.__remove}
                        searchable={false}
                    />
                </div>
            </Card>
        );
    }

    __controller() {
        if (this.state.ilKodu == "") {
            Toast.error("Lütfen il kodu giriniz!");
            return false;
        } else if (this.state.ilAdi == "") {
            Toast.error("Lütfen il adı giriniz!");
            return false;
        } else {
            return true;
        }
    }

    //noinspection JSAnnotator
    __onClick(e: Object) {
        if (this.__controller() == false) {
            return;
        }
        if (this.state.oid == "") {
            let jsonSave = {
                ilKodu: this.state.ilKodu,
                ilAdi: this.state.ilAdi
            };
            this.state.store.create(jsonSave);
            Toast.success("Veri, başarıyla eklenmiştir.");
        } else {
            let jsonUpdate = {
                oid: this.state.oid,
                ilKodu: this.state.ilKodu,
                ilAdi: this.state.ilAdi,
                version: this.state.version
            };
            this.state.store.update(jsonUpdate);
            Toast.success("Veri, başarıyla güncellenmiştir.");
        }
        this.setState({oid: ""});
        this.setState({ilKodu: ""});
        this.setState({ilAdi: ""});
        this.setState({version: ""});
        this.setState({open: !this.state.open});
    }

    //noinspection JSAnnotator
    __handleChange(e: Object) {
        let state = {};
        let value = e.target.parsedValue !== undefined ? e.target.parsedValue : e.target.value;
        state[e.target.name] = value;
        this.setState(state);
    }

    __new() {
        this.setState({oid: ""});
        this.setState({ilKodu: ""});
        this.setState({ilAdi: ""});
        this.setState({version: ""});
        this.setState({open: !this.state.open});
    }

    __edit() {
        let selectedRows = this.refs.table1.getSelectedRows();
        if (!selectedRows || !selectedRows[0]) {
            return;
        }
        this.setState({
            open: true,
            oid: selectedRows[0].oid,
            ilKodu: selectedRows[0].ilKodu,
            ilAdi: selectedRows[0].ilAdi,
            version: selectedRows[0].version
        });
        Toast.info("Veri, başarıyla seçilmiştir.");

    }

    __onCancel() {
        this.setState({oid: ""});
        this.setState({ilKodu: ""});
        this.setState({ilAdi: ""});
        this.setState({version: ""});
        this.setState({open: !this.state.open});
    }

    __remove() {
        let selectedRows = this.refs.table1.getSelectedRows();
        this.state.store.delete(selectedRows[0]);
    }

    __onKeyPress(e) {
        if (e.key == "Enter")
            this.__onClick();
    }
}