import React from "react";
import {ShallowComponent, AjaxRequest,Store,LocalEndPoint} from "robe-react-commons";
import {DataGrid} from "robe-react-ui";
import {Panel,Col,Tabs,Tab,Button,ControlLabel} from "react-bootstrap";
import SelectInput from "robe-react-ui/lib/inputs/SelectInput";
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,ResponsiveContainer} from "recharts";
import PieChart from "robe-react-ui/lib/charts/PieChart";
export default class Analiz extends ShallowComponent {

    dataIlRequest = new AjaxRequest({
        url:"ils",
        type :"GET"
    });
    dataGiderRequest = new AjaxRequest({
        url:"giderler/bcda7a0a5bc14e33abb0f3fc4d9a8e57",
        type:"GET"
    });
    dataGelirRequest = new AjaxRequest({
        url:"gelirler/bcda7a0a5bc14e33abb0f3fc4d9a8e57",
        type:"GET"
    });


    constructor(props){
        super();
        this.state = {
            dataI: [],
            dataS: [],
            dataGider : [],
            dataGelir : [],
            name :"Ulus",
            open:true
        }
    }
    render(){
        let chart = [];
        if (this.state.dataGider.length > 0){
            chart = (<div className="pttgiderler">
                <Col md={12} className="pttgiderler-secim">
                    <Col md={2}>
                        <SelectInput
                            label="İl"
                            name="ilSelect"
                            items={this.state.dataI}
                            textField="value"
                            valueField="key"
                            onChange={this.__handleChangeI}

                        />
                    </Col>
                    <Col md={2}>
                        <SelectInput
                            label="İlçe"
                            name="subeSelect"
                            items={this.state.dataS}
                            textField="value"
                            valueField="key"
                            onChange={this.__handleChangeS}

                        />
                    </Col>
                </Col>
                <br/>
                <Panel collapsible expanded={this.state.open}>
                    <Col md={12} className="pttgiderler-tabs">
                        <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                            <Tab eventKey={1} title="Toplam Gider">
                                <table className="ptttablo">
                                    <tr>
                                        <th colSpan={3}>Aktif Süre Maliyetleri</th>
                                    </tr>
                                    <tr>
                                        <th>Kabul Maliyeti</th>
                                        <th>Sevk Maliyeti</th>
                                        <th>Dağıtım Maliyeti</th>
                                    </tr>
                                    <tr>
                                        <td>85581,47</td>
                                        <td>6046,63</td>
                                        <td>0</td>
                                    </tr>
                                </table>
                                <h4>{this.state.name} Maliyetler Tablosu</h4>
                                <Col md={12} className="pttgiderler-grafik">
                                    <ResponsiveContainer height={400}>
                                        <BarChart data={this.state.dataGider}
                                                  margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                                            <XAxis dataKey="name"/>
                                            <YAxis/>
                                            <CartesianGrid strokeDasharray="10 3"/>
                                            <Tooltip/>
                                            <Legend />
                                            <Bar dataKey="atilSureMaliyet" stackId="a" fill="#8884d8" />
                                            <Bar dataKey="direkMaliyet" stackId="b" fill="#82ca9d" />
                                            <Bar dataKey="aktifSureMaliyet" stackId="b" fill="#ffc658"/>
                                        </BarChart>
                                    </ResponsiveContainer>
                                </Col>
                                <div className="pttgiderler-piecharts">
                                    <Col md={2}>
                                        <ControlLabel>Posta-Kargo Maliyeti</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[0].data}/>
                                    </Col>
                                    <Col md={2}>
                                        <ControlLabel>Banka Kabul Maliyeti</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[1].data}/>
                                    </Col>
                                    <Col md={2}>
                                        <ControlLabel>Kep Kabul Maliyeti</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[2].data}/>
                                    </Col>
                                    <Col md={2}>
                                        <ControlLabel>Kayıtsız Kabul Maliyeti</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[3].data}/>
                                    </Col>
                                    <Col md={2}>
                                        <ControlLabel>Ek-Hizmet Kabul Maliyeti</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[4].data}/>
                                    </Col>
                                    <Col md={2}>
                                        <ControlLabel>Toplam Maliyet</ControlLabel>
                                        <PieChart size={100} data={this.state.dataGider[5].data}/>
                                    </Col>
                                </div>

                            </Tab>
                            <Tab eventKey={2} title="Toplam Gelir">
                                <Panel collapsible expanded={this.state.open}>
                                    <h4>{this.state.name} Gelir Sonuç Tablosu</h4>
                                    <Col md={12} className="pttgiderler-grafik">
                                        <ResponsiveContainer height={400}>
                                            <BarChart data={this.state.dataGelir}
                                                      margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                                                <XAxis dataKey="name"/>
                                                <YAxis/>
                                                <CartesianGrid strokeDasharray="10 3"/>
                                                <Tooltip/>
                                                <Legend />
                                                <Bar dataKey="nakit" stackId="a" fill="#66ABE2" />
                                            </BarChart>
                                        </ResponsiveContainer>
                                    </Col>

                                </Panel>
                            </Tab>
                        </Tabs>
                    </Col>
                </Panel>
            </div>);
        } else {
            {this.__giderFunction()}
            {this.__gelirFunction()}
        }
        return(
            <div>{chart}</div>
        );
    }
    __giderFunction(){  //Gider Chart için ayarlama
        let state = {};
        let me = this;
        this.dataGiderRequest.call(undefined,undefined,(response) =>{
            state.dataGider = this.state.dataGider;
            this.state.dataGider = [];
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                    state.dataGider.push({
                    name: res.name,
                    aktifSureMaliyet: res.aktifSureMaliyet,
                    atilSureMaliyet : res.atilSureMaliyet,
                    direkMaliyet : res.direkMaliyet,
                    data: [{
                        unit: "TL",
                        key: "0",
                        label: "Aktif Süre Maliyeti",
                        value: res.aktifSureMaliyet
                    },{
                        unit: "TL",
                        key: "1",
                        label: "Atıl Süre Maliyeti",
                        value: res.atilSureMaliyet
                    },{
                        unit: "TL",
                        key: "2",
                        label: "Direk Maliyeti",
                        value: res.direkMaliyet
                    }]
                });
            }

            this.forceUpdate();
        })
    }
    __gelirFunction(){ //Gelir chart çekilen veriler
        let state = {};
        let me = this;
        this.dataGelirRequest.call(undefined,undefined,(response) =>{
            state.dataGelir = this.state.dataGelir;

            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.dataGelir.push({
                    name: res.name,
                    nakit : res.nakit
                });


            }
            me.setState({dataGelir:state.dataGelir});
            this.forceUpdate();
        })
    }
    __handleChangeI(e :Object){
        let state = {};
        let value = e.target.value;
        state[e.target.name] = value;

        let dataIlceRequest = new AjaxRequest({
            url:"ilces/"+value,
            type:"GET"
        });

        dataIlceRequest.call(undefined, undefined, function (response) {
            let state = {};
            state.items = response;
            this.state.dataS = [];
            state.dataS = this.state.dataS;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.dataS.push({
                    key: res.oid,
                    value: res.ilceAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        }.bind(this));
        this.setState(state);
    }
    __handleChangeS(e : Object){
        let state = {};
        let value =  e.target.value;
        state[e.target.name] = value;
        let ajaxGiderRequest = new AjaxRequest({
            url:"giderler/"+value,
            type:"GET"
        });
        let ajaxGelirRequest = new AjaxRequest({
            url:"gelirler/"+value,
            type:"GET"
        });
        ajaxGiderRequest.call(undefined, undefined, (response) => {
            state.items = response;
            this.state.dataGider = [];
            state.dataGider = this.state.dataGider;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.dataGider.push({
                    name: res.name,
                    aktifSureMaliyet: res.aktifSureMaliyet,
                    atilSureMaliyet : res.atilSureMaliyet,
                    direkMaliyet : res.direkMaliyet,
                    data: [{
                        unit: "TL",
                        key: "0",
                        label: "Aktif Süre Maliyeti",
                        value: res.aktifSureMaliyet
                    },{
                        unit: "TL",
                        key: "1",
                        label: "Atıl Süre Maliyeti",
                        value: res.atilSureMaliyet
                    },{
                        unit: "TL",
                        key: "2",
                        label: "Direk Maliyeti",
                        value: res.direkMaliyet
                    }
                    ]
                });
            }
            this.setState(state);
            this.forceUpdate();
        });
        ajaxGelirRequest.call(undefined, undefined, (response) => {
            state.items = response;
            this.state.dataGelir = [];
            state.dataGelir = this.state.dataGelir;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.dataGelir.push({
                    name: res.name,
                    nakit : res.nakit
                });
            }
            this.setState(state);
            this.forceUpdate();
        });
        this.setState(state);
    }
    componentDidMount(){
        let state = {};
        this.dataIlRequest.call(undefined, undefined, (response) => {
            state.items = response;
            state.dataI = this.state.dataI;
            for (let i = 0; i < response.length; i++) {
                let res = response[i];
                state.dataI.push({
                    key: res.oid,
                    value: res.ilAdi
                });
            }
            this.setState(state);
            this.forceUpdate();
        });

    }
}