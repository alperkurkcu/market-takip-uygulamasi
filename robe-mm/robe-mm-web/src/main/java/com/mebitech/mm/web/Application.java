package com.mebitech.mm.web;

import com.mebitech.mm.web.cli.InitialCommand;
import com.mebitech.robe.web.WebApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by kamilbukum on 03/03/2017.
 */

@ComponentScan({"com.mebitech.mm", "com.mebitech.robe"})
@EnableJpaRepositories(basePackages = {"com.mebitech.mm.repository", "com.mebitech.robe.security.db.repository"})
@EntityScan(basePackages = {"com.mebitech.mm.domain", "com.mebitech.robe.security.db.domain"})
public class Application extends WebApplication {

    @Autowired
    InitialCommand initialCommand;

    public static void main(String[] args) {
        run(Application.class, args);
    }

    @Override
    public void init(ApplicationArguments applicationArguments) {
        initialCommand.run();
    }
}
