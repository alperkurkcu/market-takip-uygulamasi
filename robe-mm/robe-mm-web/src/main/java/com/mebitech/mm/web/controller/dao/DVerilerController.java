package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.VerilerDao;
import com.mebitech.mm.domain.Veriler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 26.4.2017.
 */
@RestController
@RequestMapping(value = "dao-verilers")
@Transactional
public class DVerilerController {

    @Autowired
    private VerilerDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Veriler> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{oid}")
    public List<Veriler> getAllByMarketAndVeriTuru(@PathVariable(value = "oid") String oid,
                                                   @RequestParam(value = "prmMarketId") String prmMarketId,
                                                   @RequestParam(value = "prmTurId") String prmTurId,
                                                   @RequestParam(value = "prmYilId", defaultValue = "0") String prmYilId) {
        return daoObject.getAllByMarketAndVeriTuru(prmMarketId, prmTurId, prmYilId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Veriler create(@RequestBody Veriler object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Veriler delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Veriler update(@RequestBody Veriler object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
