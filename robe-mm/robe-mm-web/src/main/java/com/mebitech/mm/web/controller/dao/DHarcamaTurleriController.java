package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.HarcamaTurleriDao;
import com.mebitech.mm.domain.HarcamaTurleri;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@RestController
@RequestMapping(value = "dao-harcamaturleris")
@Transactional
public class DHarcamaTurleriController {

    @Autowired
    private HarcamaTurleriDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<HarcamaTurleri> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public HarcamaTurleri create(@RequestBody HarcamaTurleri object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public HarcamaTurleri delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public HarcamaTurleri update(@RequestBody HarcamaTurleri object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
