package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.VeriTurleriDao;
import com.mebitech.mm.domain.VeriTurleri;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@RestController
@RequestMapping(value = "dao-veriturleris")
@Transactional
public class DVeriTurleriController {
    @Autowired
    private VeriTurleriDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<VeriTurleri> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public VeriTurleri create(@RequestBody VeriTurleri object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public VeriTurleri delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public VeriTurleri update(@RequestBody VeriTurleri object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
