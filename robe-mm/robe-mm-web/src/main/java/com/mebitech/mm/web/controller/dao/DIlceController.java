package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.IlceDao;
import com.mebitech.mm.domain.Ilce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by KRKC on 20.4.2017.
 */
@RestController
@RequestMapping(value = "dao-ilces")
@Transactional
public class DIlceController {

    @Autowired
    private IlceDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Ilce> findAll() {
        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{oid}")
    public List<Ilce> findAllById(@PathVariable("oid") String id) {
        return daoObject.getAllByIlId(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Ilce create(@RequestBody Ilce object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Ilce delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Ilce update(@RequestBody Ilce object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }

}
