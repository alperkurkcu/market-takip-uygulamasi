package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.IlDao;
import com.mebitech.mm.domain.Il;
import com.mebitech.robe.security.db.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@RestController
@RequestMapping(value = "dao-ils")
@Transactional
public class DIlController {

    private User user;

    @Autowired
    private IlDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Il> findAll() {
        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Il create(@RequestBody Il object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Il delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Il update(@RequestBody Il object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
