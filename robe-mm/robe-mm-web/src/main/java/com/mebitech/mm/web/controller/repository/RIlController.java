package com.mebitech.mm.web.controller.repository;

import com.mebitech.mm.domain.Il;
import com.mebitech.mm.services.IlServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by KRKC on 14.4.2017.
 */
@RestController
@RequestMapping(value = "repository-ils")
public class RIlController {

    @Autowired
    private IlServices services;

    @RequestMapping(method = RequestMethod.GET)
    public List<Il> findAll() {
        return services.findAllOrderByIlKoduAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Il create(@RequestBody Il object) {
        return services.create(object);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Il update(@RequestBody Il object, @PathVariable("oid") String id) {
        return services.update(object, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Il delete(@PathVariable("oid") String id) {
        return services.delete(id);
    }

}
