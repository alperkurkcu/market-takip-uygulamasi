package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.BilgilendirmeDao;
import com.mebitech.mm.domain.Bilgilendirme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@RestController
@RequestMapping(value = "dao-bilgilendirmes")
@Transactional
public class DBilgilendirmeController {

    @Autowired
    private BilgilendirmeDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Bilgilendirme> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{oid}")
    public List<Bilgilendirme> findAllById(@PathVariable("oid") String id) {
        return daoObject.getAllByMarketId(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Bilgilendirme create(@RequestBody Bilgilendirme object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Bilgilendirme delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Bilgilendirme update(@RequestBody Bilgilendirme object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
