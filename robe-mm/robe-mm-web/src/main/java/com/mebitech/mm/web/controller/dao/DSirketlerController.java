package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.SirketlerDao;
import com.mebitech.mm.domain.Sirketler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@RestController
@RequestMapping(value = "dao-sirketlers")
@Transactional
public class DSirketlerController {
    @Autowired
    private SirketlerDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Sirketler> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Sirketler create(@RequestBody Sirketler object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Sirketler delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Sirketler update(@RequestBody Sirketler object, @PathVariable("oid") String id) {

        return daoObject.update(object);
    }
}
