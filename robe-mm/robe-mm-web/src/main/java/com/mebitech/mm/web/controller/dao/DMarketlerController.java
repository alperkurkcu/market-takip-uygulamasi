package com.mebitech.mm.web.controller.dao;

import com.mebitech.mm.dao.MarketlerDao;
import com.mebitech.mm.domain.Marketler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@RestController
@RequestMapping(value = "dao-marketlers")
@Transactional
public class DMarketlerController {
    @Autowired
    private MarketlerDao daoObject;

    @RequestMapping(method = RequestMethod.GET)
    public List<Marketler> findAll() {

        return daoObject.getAllAsc();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Marketler create(@RequestBody Marketler object) {
        return daoObject.create(object);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{oid}")
    public Marketler delete(@PathVariable("oid") String id) {
        return daoObject.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{oid}")
    public Marketler update(@RequestBody Marketler object, @PathVariable("oid") String id) {
        return daoObject.update(object);
    }
}
