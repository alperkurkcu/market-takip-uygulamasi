package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Entity
@Table(name = "tb_veriler")
@NamedQueries({
        @NamedQuery(name = "Veriler.getAllAsc", query = "SELECT c FROM Veriler c order by c.market.marketAdi asc, c.veriTuru.turAdi asc, c.kayitTarihi asc"),
        @NamedQuery(name = "Veriler.getAllByMarketAndVeriTuru", query = "SELECT c FROM Veriler c WHERE c.market.oid=:prmMarketId and c.veriTuru.oid=:prmTurId order by c.kayitTarihi asc"),
        @NamedQuery(name = "Veriler.getAllByMarketAndVeriTuruAndYear", query = "SELECT c FROM Veriler c WHERE c.market.oid=:prmMarketId and c.veriTuru.oid=:prmTurId and c.kayitTarihi between :prmStartDate and :prmFinishDate order by c.kayitTarihi asc")})
public class Veriler extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //@Column(name = "market_id")
    //private Integer marketId;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "market_oid", referencedColumnName = "oid")
    private Marketler market;

    @Column(name = "kayit_tarihi")
    private Timestamp kayitTarihi;

    //@Column(name = "kW")
    //private Integer kw;

    //@Column(name = "isi_derecesi")
    //private Integer isiDerecesi;

    @Column(name = "deger")
    private Integer deger;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "veritur_oid", referencedColumnName = "oid")
    private VeriTurleri veriTuru;

    @Transient
    private String kayitTarihiString;

    @Transient
    private String kayitTarihiDayString;

    @Transient
    private String kayitTarihiMonthString;

    @Transient
    private String kayitTarihiYearString;

    public Marketler getMarket() {
        return market;
    }

    public void setMarket(Marketler market) {
        this.market = market;
    }

    public Timestamp getKayitTarihi() {
        return kayitTarihi;
    }

    public void setKayitTarihi(Timestamp kayitTarihi) {
        this.kayitTarihi = kayitTarihi;
    }

    public String getKayitTarihiString() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        kayitTarihiString = dateFormat.format(kayitTarihi);
        return kayitTarihiString;
    }

    public VeriTurleri getVeriTuru() {
        return veriTuru;
    }

    public void setVeriTuru(VeriTurleri veriTuru) {
        this.veriTuru = veriTuru;
    }

    public Integer getDeger() {
        return deger;
    }

    public void setDeger(Integer deger) {
        this.deger = deger;
    }

    public String getKayitTarihiYearString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        kayitTarihiYearString = dateFormat.format(kayitTarihi);
        return kayitTarihiYearString;
    }

    public String getKayitTarihiMonthString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM");
        kayitTarihiMonthString = dateFormat.format(kayitTarihi);
        return kayitTarihiMonthString;
    }

    public String getKayitTarihiDayString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        kayitTarihiDayString = dateFormat.format(kayitTarihi);
        return kayitTarihiDayString;
    }
}
