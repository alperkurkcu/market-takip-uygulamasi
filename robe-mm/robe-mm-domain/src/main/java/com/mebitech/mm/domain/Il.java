package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by KRKC on 14.4.2017.
 */
@Entity
@Table(name = "tb_il")
@NamedQuery(name = "Il.getAllAsc", query = "SELECT c FROM Il c order by c.ilKodu asc")
public class Il extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "il_adi")
    private String ilAdi;

    @Column(name = "il_kodu")
    private String ilKodu;

    public String getIlAdi() {
        return ilAdi;
    }

    public void setIlAdi(String ilAdi) {
        this.ilAdi = ilAdi;
    }

    public String getIlKodu() {
        return ilKodu;
    }

    public void setIlKodu(String ilKodu) {
        this.ilKodu = ilKodu;
    }
}
