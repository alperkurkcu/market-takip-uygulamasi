package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@Entity
@Table(name = "tb_bilgilendirme")
@NamedQueries({
        @NamedQuery(name = "Bilgilendirme.getAllAsc", query = "SELECT c FROM Bilgilendirme c order by c.marketler.marketAdi asc"),
        @NamedQuery(name = "Bilgilendirme.getAllByMarketId", query = "SELECT c FROM Bilgilendirme c WHERE c.marketler.oid=:prmId order by c.turler.turAdi asc")})
public class Bilgilendirme extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "market_oid", referencedColumnName = "oid")
    private Marketler marketler;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "tur_oid", referencedColumnName = "oid")
    private HarcamaTurleri turler;

    @Column(name = "saat_araligi")
    private String saatAraligi;

    public Marketler getMarketler() {
        return marketler;
    }

    public void setMarketler(Marketler marketler) {
        this.marketler = marketler;
    }

    public HarcamaTurleri getTurler() {
        return turler;
    }

    public void setTurler(HarcamaTurleri turler) {
        this.turler = turler;
    }

    public String getSaatAraligi() {
        return saatAraligi;
    }

    public void setSaatAraligi(String saatAraligi) {
        this.saatAraligi = saatAraligi;
    }
}
