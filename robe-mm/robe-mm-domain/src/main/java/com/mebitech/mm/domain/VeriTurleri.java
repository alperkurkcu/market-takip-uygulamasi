package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@Entity
@Table(name = "tb_veri_turleri")
@NamedQuery(name = "VeriTurleri.getAllAsc", query = "SELECT c FROM VeriTurleri c order by c.turAdi asc")
public class VeriTurleri extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "tur_adi")
    private String turAdi;

    public String getTurAdi() {
        return turAdi;
    }

    public void setTurAdi(String turAdi) {
        this.turAdi = turAdi;
    }
}
