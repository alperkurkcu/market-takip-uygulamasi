package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alper.kurkcu on 21.4.2017.
 */
@Entity
@Table(name = "tb_sirketler")
@NamedQuery(name = "Sirketler.getAllAsc", query = "SELECT c FROM Sirketler c order by c.sirketAdi asc")
public class Sirketler extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "sirket_adi")
    private String sirketAdi;

    public String getSirketAdi() {
        return sirketAdi;
    }

    public void setSirketAdi(String sirketAdi) {
        this.sirketAdi = sirketAdi;
    }
}
