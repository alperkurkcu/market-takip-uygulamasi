package com.mebitech.mm.domain;


import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Entity
@Table(name = "tb_market")
@NamedQuery(name = "Marketler.getAllAsc", query = "SELECT c FROM Marketler c order by c.marketAdi asc")
public class Marketler extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "market_adi")
    private String marketAdi;

    @Column(name = "m2")
    private Integer m2;

    //@Column(name = "ilce_id")
    //private String ilceId;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ilce_oid", referencedColumnName = "oid")
    private Ilce ilce;

    //@Column(name = "sirket_id")
    //private String sirketId;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "sirket_oid", referencedColumnName = "oid")
    private Sirketler sirket;

    @Transient
    private String sirketMarketAdi;

    public String getMarketAdi() {
        return marketAdi;
    }

    public void setMarketAdi(String marketAdi) {
        this.marketAdi = marketAdi;
    }

    public Integer getM2() {
        return m2;
    }

    public void setM2(Integer m2) {
        this.m2 = m2;
    }

    public Ilce getIlce() {
        return ilce;
    }

    public void setIlce(Ilce ilce) {
        this.ilce = ilce;
    }

    public Sirketler getSirket() {
        return sirket;
    }

    public void setSirket(Sirketler sirket) {
        this.sirket = sirket;
    }

    public String getSirketMarketAdi() {

        sirketMarketAdi = sirket.getSirketAdi() + " - " + marketAdi;
        return sirketMarketAdi;
    }

}
