package com.mebitech.mm.domain;

import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by KRKC on 20.4.2017.
 */
@Entity
@Table(name = "tb_ilce")
@NamedQueries({
        @NamedQuery(name = "Ilce.getAllAsc", query = "SELECT c FROM Ilce c order by c.il.ilKodu asc, c.ilceKodu asc"),
        @NamedQuery(name = "Ilce.getAllByIlId", query = "SELECT c FROM Ilce c WHERE c.il.oid=:prmIlId order by c.ilceAdi asc")})
public class Ilce extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ilce_adi")
    private String ilceAdi;

    @Column(name = "ilce_kodu")
    private String ilceKodu;

    //@Column(name = "il_id")
    //private Integer ilId;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "il_oid", referencedColumnName = "oid")
    private Il il;

    public String getIlceAdi() {
        return ilceAdi;
    }

    public void setIlceAdi(String ilceAdi) {
        this.ilceAdi = ilceAdi;
    }

    public String getIlceKodu() {
        return ilceKodu;
    }

    public void setIlceKodu(String ilceKodu) {
        this.ilceKodu = ilceKodu;
    }

    public Il getIl() {
        return il;
    }

    public void setIl(Il il) {
        this.il = il;
    }
}
