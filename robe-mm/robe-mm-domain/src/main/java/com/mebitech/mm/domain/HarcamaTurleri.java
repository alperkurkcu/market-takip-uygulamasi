package com.mebitech.mm.domain;


import com.mebitech.robe.persistence.jpa.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Entity
@Table(name = "tb_harcama_turleri")
@NamedQuery(name = "HarcamaTurleri.getAllAsc", query = "SELECT c FROM HarcamaTurleri c order by c.turAdi asc")
public class HarcamaTurleri extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "tur_adi")
    private String turAdi;

    public String getTurAdi() {
        return turAdi;
    }

    public void setTurAdi(String turAdi) {
        this.turAdi = turAdi;
    }
}
