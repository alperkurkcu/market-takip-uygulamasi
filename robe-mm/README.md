## 1- Create schema as "maliyet_muhasebesi" on your local MySql database

## 2- Configure src/main/resources/application.yml file

        spring:
          datasource:
            url: jdbc:mysql://localhost:3306/maliyet_muhasebesi?useSSL=false
            username: your_db_username
            password: your_db_password
            dbcp2:
              validation-query: SELECT 1
              test-while-idle: true
          jpa:
            show-sql: true
            properties:
              hibernate:
                dialect: org.hibernate.dialect.MySQL5Dialect
            hibernate:
              ddl-auto: update
              naming:
                strategy: org.hibernate.cfg.ImprovedNamingStrategy


## 3- Run / Debug Configurations

        # Start Application Configuration
         - Run -> Edit Configurations -> Add New Configuration -> Application
         - Set Run/Debug name
         - Set "Single Instance Only" check
         - Select Main Class as "com.mebitech.mm.web.Application"
         - Select Use classpath of module as "robe-mm-web"
        # Init Application Configuration
         - Run -> Edit Configurations -> Add New Configuration -> Application
         - Set Run/Debug name
         - Set "Single Instance Only" check
         - Select Main Class as "com.mebitech.mm.web.Application"
         - Set Program Arguments as --init
         - Select Use classpath of module as "robe-mm-web"

## 4- Run Initial Command
    - Configure application.yml file

     hibernate:
       ddl-auto: create

    - Run init application

## 5- Run/Debug project
    - Configure application.yml file

     hibernate:
       ddl-auto: update

    - Run application