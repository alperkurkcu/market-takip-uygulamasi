package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Bilgilendirme;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@Service
public class BilgilendirmeDao extends BaseDaoImpl<Bilgilendirme, String> {

    public List<Bilgilendirme> getAllAsc() {
        return entityManager.createNamedQuery("Bilgilendirme.getAllAsc", Bilgilendirme.class)
                .getResultList();
    }

    public List<Bilgilendirme> getAllByMarketId(String oid) {
        return entityManager.createNamedQuery("Bilgilendirme.getAllByMarketId", Bilgilendirme.class).setParameter("prmId", oid)
                .getResultList();
    }
}

