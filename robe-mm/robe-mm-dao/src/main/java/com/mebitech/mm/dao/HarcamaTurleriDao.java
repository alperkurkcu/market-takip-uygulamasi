package com.mebitech.mm.dao;

import com.mebitech.mm.domain.HarcamaTurleri;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Service
public class HarcamaTurleriDao extends BaseDaoImpl<HarcamaTurleri, String> {

    public List<HarcamaTurleri> getAllAsc() {
        return entityManager.createNamedQuery("HarcamaTurleri.getAllAsc", HarcamaTurleri.class)
                .getResultList();
    }
}