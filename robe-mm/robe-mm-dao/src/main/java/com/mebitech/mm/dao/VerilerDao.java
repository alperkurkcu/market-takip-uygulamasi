package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Veriler;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by alper.kurkcu on 26.4.2017.
 */
@Service
public class VerilerDao extends BaseDaoImpl<Veriler, String> {

    public List<Veriler> getAllAsc() {
        return entityManager.createNamedQuery("Veriler.getAllAsc", Veriler.class)
                .getResultList();
    }

    public List<Veriler> getAllByMarketAndVeriTuru(String prmMarketId, String prmTurId, String prmYilId) {
        if (prmYilId.equals("undefined")) {
            return entityManager.createNamedQuery("Veriler.getAllByMarketAndVeriTuru", Veriler.class).setParameter("prmMarketId", prmMarketId).setParameter("prmTurId", prmTurId)
                    .getResultList();
        } else {
            String prmStartDate = prmYilId + "-01-01";
            String prmFinishDate = prmYilId + "-12-31";

            String query = new String();
            query = "SELECT c FROM Veriler c WHERE c.market.oid='" + prmMarketId + "' and c.veriTuru.oid='" + prmTurId + "' and c.kayitTarihi between '" + prmStartDate + "' and '" + prmFinishDate + "' order by c.kayitTarihi asc";
            Query findQuery = entityManager.createQuery(query);
            return findQuery.getResultList();
        }
    }
}
