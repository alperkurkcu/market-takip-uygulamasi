package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Ilce;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 20.4.2017.
 */
@Service
public class IlceDao extends BaseDaoImpl<Ilce, String> {

    public List<Ilce> getAllAsc() {
        return entityManager.createNamedQuery("Ilce.getAllAsc", Ilce.class)
                .getResultList();
    }

    public List<Ilce> getAllByIlId(String oid) {
        return entityManager.createNamedQuery("Ilce.getAllByIlId", Ilce.class).setParameter("prmIlId",oid)
                .getResultList();
    }
}
