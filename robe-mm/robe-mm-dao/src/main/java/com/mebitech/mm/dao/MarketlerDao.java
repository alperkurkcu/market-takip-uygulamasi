package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Marketler;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Service
public class MarketlerDao extends BaseDaoImpl<Marketler, String> {

    public List<Marketler> getAllAsc() {
        return entityManager.createNamedQuery("Marketler.getAllAsc", Marketler.class)
                .getResultList();
    }
}

