package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Il;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Service
public class IlDao extends BaseDaoImpl<Il, String> {

    public List<Il> getAllAsc() {
        return entityManager.createNamedQuery("Il.getAllAsc", Il.class)
                .getResultList();
    }
}
