package com.mebitech.mm.dao;

import com.mebitech.mm.domain.VeriTurleri;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 2.5.2017.
 */
@Service
public class VeriTurleriDao extends BaseDaoImpl<VeriTurleri, String> {

    public List<VeriTurleri> getAllAsc() {
        return entityManager.createNamedQuery("VeriTurleri.getAllAsc", VeriTurleri.class)
                .getResultList();
    }
}
