package com.mebitech.mm.dao;

import com.mebitech.mm.domain.Sirketler;
import com.mebitech.robe.persistence.jpa.dao.BaseDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alper.kurkcu on 24.4.2017.
 */
@Service
public class SirketlerDao extends BaseDaoImpl<Sirketler, String> {

    public List<Sirketler> getAllAsc() {
        return entityManager.createNamedQuery("Sirketler.getAllAsc", Sirketler.class)
                .getResultList();
    }
}