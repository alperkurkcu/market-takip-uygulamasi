package com.mebitech.mm.services;

import com.mebitech.mm.domain.Il;
import com.mebitech.mm.repository.IlRepository;
import com.mebitech.robe.persistence.jpa.services.JpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by KRKC on 14.4.2017.
 */
@Service
public class IlServices extends JpaService<Il, String> {

    private IlRepository repository;

    @Autowired
    public IlServices(IlRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public List<Il> findAllOrderByIlKoduAsc() {
        return this.findAll(sortByIlKoduAsc());
    }

    private Sort sortByIlKoduAsc() {
        return new Sort(Sort.Direction.ASC, "ilKodu");
    }
}
