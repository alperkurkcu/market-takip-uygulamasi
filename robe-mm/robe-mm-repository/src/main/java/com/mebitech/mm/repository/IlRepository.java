package com.mebitech.mm.repository;

import com.mebitech.mm.domain.Il;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by KRKC on 14.4.2017.
 */
@Repository
public interface IlRepository extends JpaRepository<Il, String> {

}
